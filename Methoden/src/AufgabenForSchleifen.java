import java.util.Scanner;

public class AufgabenForSchleifen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		aufgabe2();
	}
	
	public static void aufgabe1() {
		Scanner eingabe = new Scanner(System.in);
		
		System.out.println("Bitte gib eine Zahl ein!");
		int n = eingabe.nextInt();
		
		// a)
		for (int i =1; i<n+1; i++) {
			System.out.println(i);
		}
		
		// b)
		for (int i = n; i>0; i--) {
			System.out.println(i);
		}
	}
	
	public static void aufgabe2() {
		Scanner eingabe = new Scanner(System.in);
		
		System.out.println("Geben Sie bitte einen begrenzenden Wert ein:");
		int n = eingabe.nextInt();
		
		int c = 0;
		for (int i = 1; i<=n;i++) {
			c= c+i;
		}
		
		double f = 17.2;
		f = f/c;
		System.out.println("Die Summe für A beträgt:"+f);
		

	}

}
