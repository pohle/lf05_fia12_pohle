import java.util.Scanner;

public class Volumen {

    public static void main(String[] args) {
       
       
        ausgebenInfotext();
        double r = eingabeZahl();
        double volumen = berechnenVolumen(r);
        ausgebenErgebnis(volumen);
        System.out.println(20%6);
        
    }
    
    public static void ausgebenInfotext() {
        System.out.println("Berechnung des Volumens einer Kugel ");
        System.out.println("Geben sie dafür bitte einen Radius r ein: ");
    }
    
    public static double eingabeZahl() {
    	Scanner myScanner = new Scanner(System.in);
    	
        System.out.print("Radius: ");
        double radius = myScanner.nextDouble();
        return radius;
    }
    
    public static double berechnenVolumen(double radius) {
    	double volumen = (4.0/3.0)*Math.PI*(Math.pow(radius, 3));
    	return volumen;
    }
    
    public static void ausgebenErgebnis(double volumen) {  
        System.out.printf("Das Volumen beträgt: %.2f%n", volumen);
    }
}