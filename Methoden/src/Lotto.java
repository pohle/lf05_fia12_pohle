import java.util.Arrays;

public class Lotto {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] lottoZahlen = {3, 7, 12, 18, 37, 42};
		System.out.println(Arrays.toString(lottoZahlen));
		
		checkZahl(lottoZahlen, 12);	
		checkZahl(lottoZahlen, 13);
	}
	
	public static void checkZahl(int[] lottoList, int zahl) {
		boolean vorhanden = false;
		for (int i : lottoList) {
			if (i == zahl) {
				vorhanden = true;
			} 	
		}
		
		if (vorhanden) {
			System.out.println("Die Zahl "+zahl+" ist enthalten");
		} else {
			System.out.println("Die Zahl "+zahl+" ist nicht enthalten");
		}
	}
}
