import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte für x und y festlegen:
      // ===========================
      double x = liesDouble("Geben Sie den ersten Wert ein!");
      double y = liesDouble("Geben Sie den zweiten Wert ein!");
      double m;
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      //m = (x + y) / 2.0;
      m = berechnenMittelwert(x, y);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      //System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
      ausgebenMittelwert(x, y, m);
      
   }
   
   public static double berechnenMittelwert(double wert1, double wert2) {
 	  double erg = (wert1 + wert2)/2;
 	  return erg;
   }
   
   public static void ausgebenMittelwert(double wert1, double wert2, double mittelwert) {
	   System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", wert1, wert2, mittelwert);
	   
   }
   
   public static double liesDouble(String eingabeText) {
	   Scanner myScanner = new Scanner(System.in);
	   System.out.print(eingabeText);
	   double wert = myScanner.nextDouble();
	   return wert;
   }
}
