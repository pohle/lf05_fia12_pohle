import java.util.ArrayList;

public class Auto {
	
	
	private String marke;
	private String farbe;
	private Ladung ladung;
	private ArrayList<Ladung> ladungen = new ArrayList<Ladung>();

	public Auto(String marke, String farbe) {
		this.marke = marke;
		this.farbe = farbe;
	}
	
	public Auto() {
		
	}
	
	
	public void fahre(int strecke) {
		System.out.println("Auto fährt "+strecke+"km");
	}
	
	public void tanke(double liter) {
		System.out.println("Auto wurde für "+liter+" aufgetankt");
		
	}
	
	public void setMarke(String marke) {
		this.marke = marke;
	}
	
	public String getMarke() {
		return marke;
	}
	
	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}
	
	public String getFarbe() {
		return farbe;
	}

	public Ladung getLadung() {
		return ladung;
	}

	public void setLadung(Ladung ladung) {
		this.ladung = ladung;
	}
	
	
}
