package Raumschiff;

import java.util.ArrayList; import java.lang.Math;

/**
 * 
 * @author Pohle 
 * @since 
 *
 */

public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikation = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	public Raumschiff() {
		
	}
	
/**
 * Voll-Parametisierter Konstruktor wird definiert 
 * */
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}
	
	/**
	 * eine Zufallszahl wird erzeugt und mit dem rückgabewert int zurückgegeben
	 * */
	public int erzeugenZufallszahl(int min, int max) {
		int range = (max-min);
		int zufallszahl = (int)(Math.random()*range)+min;
		
		return zufallszahl;
	}
	
	/**
	 * Prozentuale Berechnung der reparierten Schiffsstrukturen
	 * */
	public int errechnenSchiffreperatur(int zufallszahl, int anzahlDroiden, int anzahlSchiffsstrukturen) {
		int ergebnis = (int)Math.round((zufallszahl*anzahlDroiden)/anzahlSchiffsstrukturen);
		return ergebnis;
	}
	
	/**
	 * Die Methode entscheidet anhand der übergebenen Parameter, welche Schiffsstrukturen repariert werden sollen.
	Es wird eine Zufallszahl zwischen 0 - 100 erzeugt, welche für die Berechnung der Reparatur benötigt wird.
	Ist die übergebene Anzahl von Androiden größer als die vorhandene Anzahl von Androiden im Raumschiff, dann wird die vorhandene Anzahl von Androiden eingesetzt
	Die Methode errechnen Schiffsreperatur wird aufgerufen und es wird eine Prozentuale Berechnung der reparierten Schiffsstrukturen durchgeführt
	Das Ergebnis wird den auf true gesetzten Schiffsstrukturen hinzugefügt.
	 * */
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle , int anzahlDroiden) {
		int zufallszahl = erzeugenZufallszahl(0, 100);
		boolean[] schiffsstrukturen = {schutzschilde, energieversorgung, schiffshuelle};
		int ergebnis;
		
		int auswahlStrukturen = 0;
		for (boolean e : schiffsstrukturen) {
			if (e) {
				auswahlStrukturen ++;
			}
		}
		

		if (anzahlDroiden > androidenAnzahl) {
			ergebnis = errechnenSchiffreperatur(zufallszahl, androidenAnzahl, auswahlStrukturen);
		} else {
			ergebnis = errechnenSchiffreperatur(zufallszahl, anzahlDroiden, auswahlStrukturen);
		}
		
		if (schutzschilde) {
			setSchildeInProzent(getSchildeInProzent()+ergebnis);
		}
		if (schiffshuelle) {
			setHuelleInProzent(getHuelleInProzent()+ergebnis);
		}
		if (energieversorgung) {
			setEnergieversorgungInProzent(getEnergieversorgungInProzent()+ergebnis);
		}
		
	}
	
	/**
	 * Wenn die Menge einer Ladung 0 ist, dann wird das Objekt Ladung aus der Liste entfernt.
	 */
	
	public void ladungsverzeichnisAufraeumen() {
		for (int i = 0; i<ladungsverzeichnis.size(); i++) {
			if (ladungsverzeichnis.get(i).getMenge() == 0) {
				ladungsverzeichnis.remove(i);
			}
		}
	}
	
	/**
	 * 
        Gibt es keine Ladung Photonentorpedo auf dem Schiff, wird als Nachricht Keine Photonentorpedos gefunden! in der Konsole ausgegeben und die Nachricht an alle -=*Click*=- ausgegeben.
        Ist die Anzahl der einzusetzenden Photonentorpedos größer als die Menge der tatsächlich Vorhandenen, so werden alle vorhandenen Photonentorpedos eingesetzt.
        Ansonsten wird die Ladungsmenge Photonentorpedo über die Setter Methode vermindert und die Anzahl der Photonentorpedo im Raumschiff erhöht.
        Konnten Photonentorpedos eingesetzt werden, so wird die Meldung [X] Photonentorpedo(s) eingesetzt auf der Konsole ausgegeben. [X] durch die Anzahl ersetzen.
	 * @param anzahlTorpedos
	 */
	
	public void photonentorpedosLaden(int anzahlTorpedos) {
			int existiert = 0;
			for (int i=0; i<ladungsverzeichnis.size(); i++) {
				if (ladungsverzeichnis.get(i).getBezeichnung().equals("Photonentorpedo")) {
					if (anzahlTorpedos > ladungsverzeichnis.get(i).getMenge()) {
						photonentorpedoAnzahl += ladungsverzeichnis.get(i).getMenge();
						System.out.println(ladungsverzeichnis.get(i).getMenge()+" Photonentorpedo(s) eingesetzt");
						ladungsverzeichnis.get(i).setMenge(0);
					} else {
						photonentorpedoAnzahl += anzahlTorpedos;
						ladungsverzeichnis.get(i).setMenge(ladungsverzeichnis.get(i).getMenge()-anzahlTorpedos);
						System.out.println(anzahlTorpedos+" Photonentorpedo(s) eingesetzt");
					}
				} else {
					existiert += 1;
				}
			}
			if (existiert == ladungsverzeichnis.size()) {
				System.out.println("Keine Photonentorpedos gefunden!");
				System.out.println("-=*Click*=-");
			}
	}
	
	/**
	 * Gibt den broadcastKommunikator zurück
	 * @return
	 */
	
	public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return broadcastKommunikation;
	}
	
	/**
	 * Die Nachricht wird dem broadcastKommunikator hinzugefügt
	 * @param message
	 */
	
	public void nachrichtAnAlle(String message) {
		broadcastKommunikation.add(message);
	}
	
	/**
	 * Gibt es keine Torpedos, so wird als Nachricht an Alle -=*Click*=- ausgegeben.
	 * Ansonsten wird die Torpedoanzahl um eins reduziert und die Nachricht an Alle Photonentorpedo abgeschossen gesendet. Außerdem wird die Methode Treffer aufgerufen
	 * @param r
	 */
	
	public void photonentorpedoSchiessen(Raumschiff r) {
		if (getPhotonentorpedoAnzahl() == 0) {
			System.out.println("-=*Click*=-");
		} else {
			photonentorpedoAnzahl -= 1;
			System.out.println("Photonentorpedo auf Raumschiff \""+r.getSchiffsname()+"\" abgeschossen");
			treffer(r);
		}
	}
	
	/** 
	 * Ist die Energieversorgung kleiner als 50%, so wird als Nachricht an Alle Click ausgegeben.
	 * Ansonsten wird die Energieversorgung um 50% reduziert und die Nachricht an Alle Phaserkanone abgeschossen gesendet. Außerdem wird die Methode Treffer aufgerufen
	 * @param r
	 */
	
	public void phaserkanoneSchiessen(Raumschiff r) {
		if (getEnergieversorgungInProzent() < 50) {
			System.out.println("-=*Click*=-");
		} else {
			energieversorgungInProzent -= 50;
			System.out.println("Phaserkanone auf Raumschiff \""+r.getSchiffsname()+"\" abgeschossen");
			treffer(r);
		}
	}
	
	/** 
	 * Die Nachricht [Schiffsname] wurde getroffen! wird in der Konsole ausgegeben. [Schiffsname] durch den Namen des Schiffes ersetzen
	 * Die Schilde des getroffenen Raumschiffs werden um 50% geschwächt.
	 * Sollte anschließend die Schilde vollständig zerstört worden sein, so wird der Zustand der Hülle und der Energieversorgung jeweils um 50% abgebaut.
	 * Sollte danach der Zustand der Hülle auf 0% absinken, so sind die Lebenserhaltungssysteme vollständig zerstört und es wird eine 
	 * Nachricht an Alle ausgegeben, dass die Lebenserhaltungssysteme vernichtet worden sind.
	 * */
	
	private void treffer(Raumschiff r) {
		System.out.printf("%s wurde getroffen!\n", r.getSchiffsname());
		
		r.schildeInProzent -= 50;
		if (r.getSchildeInProzent() <= 0) {
			r.huelleInProzent -= 50;
			r.energieversorgungInProzent -= 50;
		}
		
		if (r.getHuelleInProzent() <= 0) {
			setLebenserhaltungssystemeInProzent(0);
			nachrichtAnAlle("Lebenserhaltungsysteme von Raumschiff "+r.getSchiffsname()+" wurden vernichtet!");
			System.out.printf("Lebenserhaltungssysteme von %s wurden vernichtet!\n", r.getSchiffsname());
		}
	}
	
	/**
	 * Alle Ladungen eines Raumschiffes auf der Konsole mit Ladungsbezeichnung und Menge ausgeben.
	 */
	
	public void ladungsverzeichnisAusgeben() {
		System.out.println("Ladungsverzeichnis des Raumschiffs \""+getSchiffsname()+"\":");
		for (int i=0; i<ladungsverzeichnis.size(); i++) {
			System.out.printf("%s: %d \n", ladungsverzeichnis.get(i).getBezeichnung(), ladungsverzeichnis.get(i).getMenge());

		}
		
	}
	
	/** 
	 * eine neue Ladung wird dem Ladungsverzeichnis hinzugefügt
	 * @param neueLadung
	 */
	
	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}
	
	
	/** 
	 * Alle Zustände (Attributwerte) des Raumschiffes auf der Konsole mit entsprechenden Namen ausgeben
	 * */
	public void zustandRaumschiff() {
		System.out.println("Zustand des Raumschiffs \""+getSchiffsname()+ "\":");
		System.out.printf("Energieversorgung: %d \nSchilde: %d \nHuelle: %d \nLebenserhaltungssysteme: %d \n", getEnergieversorgungInProzent(), getSchildeInProzent(), getHuelleInProzent(), getLebenserhaltungssystemeInProzent());
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public ArrayList<String> getBroadcastKommunikation() {
		return broadcastKommunikation;
	}

	public void setBroadcastKommunikation(ArrayList<String> broadcastKommunikation) {
		this.broadcastKommunikation = broadcastKommunikation;
	}

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	
	
	
}
