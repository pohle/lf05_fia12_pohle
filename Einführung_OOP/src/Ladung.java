
public class Ladung {
	private String art;
	private int anzahl;
	
	public Ladung() {
		
	}
	
	public Ladung(String art, int anzahl) {
		this.art = art;
		this.anzahl = anzahl;
	}

	public String getArt() {
		return art;
	}

	public void setArt(String art) {
		this.art = art;
	}

	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
	
	
}
