
public class Ausgabeformatierung2 {

	public static void main(String[] args) {
		//AUfgabe 1
		String s = "*";
		
		System.out.printf("%6s"+s, s);
		System.out.printf("\n%4s"+"%5s", s,s);
		System.out.printf("\n%4s"+"%5s", s,s);
		System.out.printf("\n%6s"+s, s);
		
		//Aufgabe 2
		int eins = 1;
		int zwei = 2;
		int drei = 3;
		int vier = 4;
		int fünf = 5;
		
		System.out.printf("\n%-5s="+"%-19s="+"%4s", eins+"!", " "+eins, 1);
		System.out.printf("\n%-5s="+"%-19s="+"%4s", zwei+"!", " "+eins+"*"+zwei, 2);
		System.out.printf("\n%-5s="+"%-19s="+"%4s", drei+"!", " "+eins+"*"+zwei+"*"+drei, 6);
		System.out.printf("\n%-5s="+"%-19s="+"%4s", vier+"!", " "+eins+"*"+zwei+"*"+drei+"*"+vier, 24);
		System.out.printf("\n%-5s="+"%-19s="+"%4s", fünf+"!", " "+eins+"*"+zwei+"*"+drei+"*"+vier+"*"+fünf, 120);
		
		//Aufgabe 3 fertigg
		System.out.printf("\n%-12s|%10s", "Fahrenheit","Celsius");
		System.out.print("\n----------------------");
		System.out.printf("\n%-12s|%10.2f", -20, -28.8889);
		System.out.printf("\n%-12s|%10.2f", -10, -23.3333);
		System.out.printf("\n%-12s|%10.2f",   0, -17.7778);
		System.out.printf("\n%-12s|%10.2f",  20, -6.6667);
		System.out.printf("\n%-12s|%10.2f",  30, -1.1111);
		
	}

}
