
public class HelloWorld {

	public static void main(String[] args) {
		String name = "Jatuh";
		
		// System.out.println("Das ist ein Beispielsatz! \nHallo Sir \""+ name + "\"");
		//print() gibt alles in einer Zeile aus, println() leitet zeilenumbrüche ein
		
		String stern = "*";
		
		System.out.printf("%6s%n", stern);
		System.out.printf("%5s"+ stern + stern, stern);
		System.out.printf("\n%4s"+ stern + stern + stern + stern, stern);
		System.out.printf("\n%3s"+ stern + stern + stern + stern + stern + stern, stern);
		System.out.printf("\n%2s"+ stern + stern + stern + stern + stern + stern + stern + stern, stern);
		System.out.printf("\n%5s"+ stern + stern, stern);
		System.out.printf("\n%5s"+ stern + stern, stern);
		
	
		double[] d = new double[5];
		d[0] = 22.4234234;
		d[1] = 111.2222;
		d[2] = 4.0;
		d[3] = 1000000.551;
		d[4] = 97.34;
		
		for(int i=0; i<d.length; i++) {
		System.out.printf("\n%.2f\n", d[i]);
		}
		
	}

}
