﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	boolean moreTickets = true;
    	
    	
    	while (moreTickets) {
	    	double zahlenderBetrag = fahrkartenbestellungErfassen();
	    	double gesamtBetrag = fahrkartenBezahlen(zahlenderBetrag);
	    	fahrkartenAusgeben();
	    	rueckgeldAusgeben(gesamtBetrag, zahlenderBetrag);
	    	moreTickets = kaufenWeitereTickets(moreTickets);
    	}
    }
    
    public static int[][] verwaltenKasse() {
    	int[][] kasse = {{1,20},{2,20},{5,20},{10,20},{20,20},{50,20},{100,20},{200,20}};
    	
    	System.out.println("Fahrkarten Kasse:\n");
		System.out.println("Auswahlnummer       Geldmuenzen             Anzahl    Summe\n");
    	for (int i = 0; i<kasse.length;i++) {
    		
    		System.out.printf("%-20d %-15d %10d \n", i+1, kasse[i][0], kasse[i][1]);
    	}
    	
    	return kasse;
    	}
    
    public static boolean kaufenWeitereTickets(boolean moreTickets) {
    	Scanner tastatur = new Scanner(System.in);
    	
    	System.out.println("\nWillst du weitere Tickets kaufen?  ja/nein");
    	String eingabeJaNein = tastatur.nextLine().toLowerCase();
    	
    	if (eingabeJaNein.equals("ja")) {
    		moreTickets = true;
    	} else {
    		moreTickets = false;
    		System.out.println("Schönen Tag noch!");
    	}
    	return moreTickets;
    }
    
    public static int validInputInt() {
    	Scanner tastatur = new Scanner(System.in);
    	int p = 0;
    	boolean valid = false;
    	while (!valid) {
 	        if (tastatur.hasNextInt()) {
 	        	p = tastatur.nextInt(); 
 	        	valid = true;
 	        } else {
 	        	tastatur.next();
 	        	System.out.println("Nur Zahlen sind gültig!");
 	        }
         }
    	return p;
    }
    

    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	
    	boolean bezahlvorgangAbbrechen = false;
    	double zuZahlenderBetrag = 0;
    	
    	int[][] fahrkartenKasse = verwaltenKasse();
    	
    	int[] fahrkartenNummern = {1 ,2 ,3, 4, 5, 6, 7, 8, 9 ,10, 11, 69};
    	String[] fahrkartenListe = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
    	double[] fahrkartenPreiselist = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
    	
    	while(!bezahlvorgangAbbrechen) {
    		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin aus:\n");
    		System.out.println("Auswahlnummer       Bezeichnung                      Preis in Euro\n");
	    	for (int i = 0; i<fahrkartenListe.length;i++) {
	    		System.out.printf("%-15d %-35s %10.2f \n", i+1, fahrkartenListe[i], fahrkartenPreiselist[i]);
	    	}
	    	System.out.println("11 \t\tBezahlen\n");
	    	System.out.println("69 \t\tAdministrator Menue\n");
	    	int optionKarte = 0;
	        
	        optionKarte = validInputInt();
	        while ((optionKarte>11 || optionKarte<=0 || optionKarte !=69)) {
	        	System.out.println("Falsche Eingabe");
	        	optionKarte = validInputInt();
	        }
	        
	        if (optionKarte == 11) {
	        	bezahlvorgangAbbrechen = true;
	        } else if (optionKarte == 69) {
	        	verwaltenKasse();
	        } else {
	        	zuZahlenderBetrag += fahrkartenPreiselist[optionKarte-1]*anzahlTicketAbfrage();
		        System.out.printf("\nZwischensumme beträgt: %.2f\n", zuZahlenderBetrag);
	        }
	        
    	}
    	
        return zuZahlenderBetrag;
    }
    
    public static int anzahlTicketAbfrage() {
    	System.out.println("Anzahl der Tickets: ");
        
        int anzahlTickets = validInputInt();
        while (anzahlTickets>10 || anzahlTickets<=0) {
    		System.out.println("Es ist nur eine Zahl bis 10 gültig! Sie haben etwas ungültiges eingegeben!");
    		anzahlTickets = validInputInt();
    	}
        return anzahlTickets;
    }
    
    public static double fahrkartenBezahlen(double zuZahlen) {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag = 0.0;
    	while (eingezahlterGesamtbetrag < zuZahlen) {
    		System.out.printf("\n noch zu zahlen: %.2f Euro", zuZahlen-eingezahlterGesamtbetrag);
    		
    		System.out.print("\nEingabe (mind. 5Ct, höchstens 2 Euro): ");
     	    double eingeworfeneMünze = tastatur.nextDouble();
     	    if (eingeworfeneMünze <= 2.0) {
     	    	eingezahlterGesamtbetrag += eingeworfeneMünze;
     	    }
     	    else {
     	    	System.out.println("\ndu kannst höchstens 2 Euro einwerfen!");
     	    }
    	}
    	return eingezahlterGesamtbetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	 System.out.println("\nFahrschein wird ausgegeben");
         for (int i = 0; i < 8; i++)
         {
            System.out.print("=");
            warte(250);
         }
         System.out.println("\n\n");
    }
    
    public static void warte(int millisekunden) {
    	try {
  			Thread.sleep(millisekunden);
  		} catch (InterruptedException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
    }
    
    public static void rueckgeldAusgeben(double gesamtBetrag, double zuZahlen) {
    	double rückgabebetrag = gesamtBetrag - zuZahlen;
	        if(rückgabebetrag > 0.0)
	        {
	     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO", rückgabebetrag);
	     	   System.out.println("\nwird in folgenden Münzen ausgezahlt:");
	
	            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	            {
	            	muenzeAusgeben(2, "Euro");
	            	rückgabebetrag -= 2.0;
	            }
	            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	            {
	              muenzeAusgeben(1, "Euro");
	 	          rückgabebetrag -= 1.0;
	            }
	            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	            {
	              muenzeAusgeben(50, "CENT");
	 	          rückgabebetrag -= 0.5;
	            }
	            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	            {
	              muenzeAusgeben(20, "CENT");
	  	          rückgabebetrag -= 0.2;
	            }
	            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	            {
	              muenzeAusgeben(10, "CENT");
	 	          rückgabebetrag -= 0.1;
	            }
	            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	            {
	              muenzeAusgeben(5, "CENT");
	  	          rückgabebetrag -= 0.05;
	            }
	        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
    	
    }
    
    public static void muenzeAusgeben(int betrag, String einheit) {
    	System.out.printf("   ****\n *      *\n*   %-2s   * \n*  %s  * \n *      * \n   ****\n", betrag, einheit);
    }

}